$.extend(window.lang_hu, {
"help_welcome": "Üdvözöllek a 'Vagy inkább' kifejezésre, a <b> megosztásra </ b> bemutatóra? <br> Ez a bemutató néhány egyszerű lépésben megmutatja <b> hogyan használhatja </ b> ezt az alkalmazást!",
"help_first": "Először <b> beillesztés </ b> szöveget a <b> első </ b> lehetőségre.",
"help_second": "Ezután írja be a <b> Insert </ b> szöveget a <b> second </ b> opcióra.",
"help_share": "Kattintson a <b> megosztás </ b> gombra!",
"help_shareDone": "És <b> megosztani </ b> <b> bárhol </ b>!",
"help_playFunctionality": "Ha szeretné <b> Play </ b> a <b> Játék </ b> -ot is inkább ...",
"help_play": "Kattintson a <b> Lejátszás </ b> gombra a fejlécben ...",
"help_playScreen": "És <b> indítsa el </ b> a <b> játékhoz </ b>! <br> <br> (Kattintson az 'új' gombra a fejlécben, hogy visszatérjen a szavazások megosztásához)",
"help_end": "Köszönjük <b> befejezése </ b> a <b> bemutatót </ b> :)",
});