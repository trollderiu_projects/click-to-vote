$.extend(window.lang_af, {
"help_welcome": "Welkom by die tutoriaal van: Wil jy eerder?, Vir <b> deel! </ B>. <br> Hierdie handleiding wys jou <b> hoe om </ b> hierdie program in paar eenvoudige stappe te gebruik!",
"help_first": "Eerstens, <b> voeg </ b> teks in die opsie <b> eerste </ b>.",
"help_second": "Dan, <b> Voeg </ b> teks in die opsie <b> tweede </ b>.",
"help_share": "En klik op die knoppie <b> deel </ b>!",
"help_shareDone": "En jy kan <b> deel </ b> dit <b> oral </ b>!",
"help_playFunctionality": "As jy ook <b> Speel </ b> die <b> Speletjie </ b> wil, wil jy eerder ...",
"help_play": "Klik op die knoppie <b> Speel </ b> op die kop ...",
"help_playScreen": "En <b> begin </ b> om <b> te speel </ b>! <br> <br> (Klik op 'nuwe' knoppie op header om terug te gaan vir deel jou stembusse)",
"help_end": "Dankie vir <b> afronding </ b> die <b> handleiding </ b> :)",
});