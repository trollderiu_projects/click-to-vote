$.extend(window.lang_az, {
"help_welcome": "<B> paylaşım! </ B> üçün öğretici səhifəsinə xoş gəlmisiniz: Daha çox istəyirsiniz? <br> Bu təlimat bir neçə sadə addımda bu tətbiqin <b> necə istifadə ediləcəyini </ b> göstərəcək!",
"help_first": "İlk <b> ilk </ b> seçeneğine <b> daxil edin </ b> mətn.",
"help_second": "Sonra <b> ikinci </ b> seçimində <b> Daxil et </ b> mətni.",
"help_share": "<B> paylaş </ b> 'düyməsinə basın!",
"help_shareDone": "<B> <b> hər hansı bir yerdə </ b> <b> paylaşa bilərsiniz </ b>!",
"help_playFunctionality": "Əgər <b> Çal </ b> 'nı istəsəniz <b> Oyun </ b>",
"help_play": "Başlıqdakı <b> Çal </ b> 'düyməsinə basın ...",
"help_playScreen": "<B> play </ b> üçün <b> başlayın </ b>! <br> <br> (sandığa bölüşmək üçün geri dönmək üçün başlıqda 'yeni' düyməsinə vurun)",
"help_end": "<B> finiş </ b> üçün <b> tutorial </ b> :) üçün təşəkkür edirik",
});