$.extend(window.lang_gl, {
"help_welcome": "Benvidos ao tutorial de: ¿Preferirías ?, para <b> compartir! </ B>. <br> Este tutorial mostraralle <b> como usar </ b> esta aplicación en poucos pasos simples.",
"help_first": "Primeiro, o texto <b> Inserir </ b> na opción <b> primeiro </ b>.",
"help_second": "Logo, <b> Inserir </ b> texto na <b> segunda </ b> opción.",
"help_share": "E faga clic no botón <b> compartir </ b>.",
"help_shareDone": "E podes <b> compartir </ b> nel <b> en calquera lugar </ b>.",
"help_playFunctionality": "Se tamén queres <b> Jugar </ b> o <b> Xogo </ b> de ti, antes ...",
"help_play": "Fai clic no botón <b> Xogar </ b> do encabezado ...",
"help_playScreen": "E <b> comezar </ b> a <b> xogar </ b>. <br> <br> (Fai clic no botón 'novo' no encabezado para volver a compartir as túas enquisas)",
"help_end": "Grazas por <b> terminar </ b> o <b> tutorial </ b> :)",
});