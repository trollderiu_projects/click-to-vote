$.extend(window.lang_ja, {
"help_welcome": "<b>共有！</ b>のチュートリアルへようこそ。 <br>このチュートリアルでは、このアプリケーションを簡単な手順で<b>使用方法</ b>する方法を紹介します。",
"help_first": "まず、<b>最初の</ b>オプションにテキストを<b>挿入</ b>します。",
"help_second": "次に、<b> 2番目の</ b>オプションにテキストを<b>挿入</ b>します。",
"help_share": "[<b>共有</ b>]ボタンをクリックしてください。",
"help_shareDone": "<b>どこでも</ b> <b>共有</ b>することができます。",
"help_playFunctionality": "また、あなたの<b>ゲーム</ b>を<b>再生</ b>したい場合は...",
"help_play": "ヘッダーの[<b>再生</ b>]ボタンをクリックします。",
"help_playScreen": "<b>開始</ b>して<b>再生</ b>します。 <br> <br>（あなたの世論調査を共有するために戻るにはヘッダーの「新規」ボタンをクリックしてください）",
"help_end": "<b>チュートリアル</ b>を<b>終了</ b>していただきありがとうございます:)",
});