$.extend(window.lang_tr, {
"help_welcome": "Öğretici Kitabına hoş geldiniz: '<b> paylaş! </ B> için, Yerine Öyley misiniz?' <br> Bu eğitim size birkaç basit adımda bu Uygulamayı <b> nasıl kullanacağınızı </ b> gösterecektir!",
"help_first": "Önce <b> ilk </ b> seçeneğine <b> eklemek </ b> metin ekleyin.",
"help_second": "Ardından, <b> ikinci </ b> seçeneğine <b> Ekle </ b> metni ekleyin.",
"help_share": "Ve <b> paylaş </ b> düğmesini tıklayın!",
"help_shareDone": "Ve siz <b> her yerde </ b> paylaşabilirsiniz </ b>!",
"help_playFunctionality": "Ayrıca <b> Oyna </ b> 'na <b> oynamak </ b> isterseniz ...",
"help_play": "Üstbilgideki <b> Oyna </ b> düğmesini tıklayın ...",
"help_playScreen": "Ve <b> başlatmak </ b> için <b> oynamak </ b>! <br> <br> (Anketlerinizi paylaşmak için geri dönmek için başlığın 'yeni' düğmesine tıklayın)",
"help_end": "<B> tamamlandı </ b> <b> öğretici </ b> için teşekkür ederiz :)",
});