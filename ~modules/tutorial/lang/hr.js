$.extend(window.lang_hr, {
"help_welcome": "Dobro došli u tutorial: Želite li radije ?, za <b> dijelite! </ B>. <br> Ovaj vodič će vam pokazati <b> kako upotrebljavati </ b> ovu aplikaciju u nekoliko jednostavnih koraka!",
"help_first": "Prvo, <b> umetnite </ b> tekst na <b> prvi </ b> opciju.",
"help_second": "Zatim stavite <b> Umetni </ b> tekst u <b> drugu </ b> opciju.",
"help_share": "I kliknite gumb <b> Podijeli </ b>!",
"help_shareDone": "I možete <b> dijeliti </ b> bilo <b> bilo gdje </ b>!",
"help_playFunctionality": "Ako želite i <b> igrati </ b> <b> Igra </ b> od Birača ...",
"help_play": "Kliknite gumb <b> Igraj </ b> na zaglavlju ...",
"help_playScreen": "I <b> započnite </ b> za <b> igrati </ b>! <br> <br> (Kliknite 'novo' gumb na zaglavlju da biste se vratili za dijeljenje anketa)",
"help_end": "Hvala vam za <b> završiti </ b> <b> udžbenik </ b> :)",
});