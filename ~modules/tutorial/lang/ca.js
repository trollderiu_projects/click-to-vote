$.extend(window.lang_ca, {
"help_welcome": "Et donem la benvinguda al tutorial de: Voleu, al contrari, per <b> compartir! </ B>. <br> Aquest tutorial us mostrarà <b> com s'utilitza </ b> aquesta aplicació en pocs passos senzills.",
"help_first": "Primer, <b> insereix </ b> text a l'opció <b> primer </ b>.",
"help_second": "A continuació, <b> Insereix </ b> text a l'opció <b> segon </ b>.",
"help_share": "I feu clic al botó <b> compartir </ b>.",
"help_shareDone": "I podeu <b> compartir </ b> que <b> enlloc </ b>.",
"help_playFunctionality": "Si també voleu <b> Jugar </ b>, el <b> Joc </ b> de Voldria ...",
"help_play": "Feu clic al botó <b> Jugar </ b> a la capçalera ...",
"help_playScreen": "I <b> començar </ b> a <b> jugar </ b>! <br> <br> (Feu clic a 'nou' a la capçalera per tornar a compartir les vostres enquestes)",
"help_end": "Gràcies per <b> acabar </ b> el <b> tutorial </ b> :)",
});