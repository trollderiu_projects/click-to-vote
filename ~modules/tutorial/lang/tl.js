$.extend(window.lang_tl, {
"help_welcome": "Maligayang pagdating sa tutorial ng: Gusto mo ba ?, para sa <b> magbahagi! </ B>. <br> Ang tutorial na ito ay magpapakita sa iyo ng <b> kung paano gamitin </ b> ang App na ito sa ilang mga simpleng hakbang!",
"help_first": "Una, <b> magsingit </ b> ng teksto sa opsyon na <b> una </ b>.",
"help_second": "Pagkatapos, <b> Magsingit </ b> sa opsyon na <b> segundo </ b>.",
"help_share": "At mag-click sa pindutang <b> magbahagi </ b>!",
"help_shareDone": "At maaari mong <b> magbahagi </ b> ito <b> kahit saan </ b>!",
"help_playFunctionality": "Kung gusto mo ring <b> I-play </ b> ang <b> Laro </ b> ng Gusto mo ...",
"help_play": "Mag-click sa pindutan ng <b> I-play </ b> sa header ...",
"help_playScreen": "At <b> simulan </ b> sa <b> play </ b>! <br> <br> (Mag-click sa pindutang 'bagong' sa header upang bumalik para ibahagi ang iyong mga botohan)",
"help_end": "Salamat sa <b> tapusin </ b> ang <b> tutorial </ b> :)",
});