
FillTable.prototype.emptyPoll = function () {
    console.log("empty poll");

    var question = this.addTitle();
    question.attr("contenteditable", "true");
    this.$div.find(".options").append(question);

    var answer1 = this.addCell(0);
    answer1.find(".option_text .text").attr("contenteditable", "true");
    this.$div.find(".options").append(answer1);

    var answer2 = this.addCell(1);
    answer2.find(".option_text .text").attr("contenteditable", "true");
    this.$div.find(".options").append(answer2);

    this.$div.find(".option").click(function () {
        $(this).find(".text").focus();
    });

    //try to prevent scroll move on focus:
    this.$div.find(".option_text").on('focusin focus', function (e) {
        e.preventDefault();
    });
};

FillTable.prototype.addTitle = function () {
    return $("<div class='question' placeholder='" + transl("questionPlaceholder") + "'>")
};

//HORIZONTAL GRADIENT BACKGROUND
FillTable.prototype.updateOptionStyle = function (option, perc) {
    var option_div = this.$div.find(".option_" + option[0]);
    option_div.find(".percentage").text(perc);
    option_div.find(".background").css("width", perc + "%");
};
